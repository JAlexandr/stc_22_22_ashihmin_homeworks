public class Product {
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCost() {
        return cost;
    }

    public Integer getCount() {
        return count;
    }

    private Integer id;
    private String name;
    private String cost;
    private Integer count;


    public void setName(String name) {
        this.name = name;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost='" + cost + '\'' +
                ", count=" + count +
                '}';
    }

    public Product(Integer id, String name, String cost, int count) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.count = count;
    }
}