public class Main {

    public static void main(String[] args) {
        ProductsRepository productRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        try {
            System.out.println(productRepository.findById(1));
            System.out.println(productRepository.findAllByTitleLike("ic"));
            Product product = productRepository.findById(2);
            product.setName("Bread");
            product.setCost("120");
            product.setCount(10);
            productRepository.update(product);
            System.out.println(productRepository.findById(2));

        } catch (RuntimeException e) {
            System.out.println("Error");
        }
    }
}