import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        String cost = parts[2];
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, name, cost, count);

    };
    private static final Function<Product, String> productToStringMapper = product -> {
        return product.getId().toString() + "|" + product.getName() + "|" + product.getCost() + "|" + product.getCount();
    };


    public ProductsRepositoryFileBasedImpl(String file) {
        this.fileName = file;
    }

    @Override
    public Product findById(Integer id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.getId().equals(id))
                    .findFirst().orElseGet(() -> new Product(0, "0", "0", 0));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.getName().toLowerCase().contains(title.toLowerCase()))
                    .findFirst().orElseGet(() -> new Product(0, "0", "0", 0));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }


    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            Product oldProduct = productList.stream().filter(it -> it.getId().equals(product.getId())).findFirst().orElseThrow();
            Product newProduct = new Product(oldProduct.getId(), product.getName(), product.getCost(), product.getCount());
            List<Product> products = productList.stream().map(it -> {
                if (Objects.equals(it.getId(), newProduct.getId())) {
                    return newProduct;
                }
                return it;
            }).toList();
            saveAll(products);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean save(Product product) throws UnsuccessfuWorkWithFileExeption {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = productToStringMapper.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            return false;
        }
        return true;
    }

    public void saveAll(List<Product> products) throws UnsuccessfuWorkWithFileExeption {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringProduct = new StringBuilder();
            {
                for (Product product : products)
                    stringProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfuWorkWithFileExeption(e);
        }
    }
}