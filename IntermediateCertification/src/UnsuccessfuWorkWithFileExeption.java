public class UnsuccessfuWorkWithFileExeption extends RuntimeException {
    public UnsuccessfuWorkWithFileExeption(Exception e) {
        super(e);
    }

    public UnsuccessfuWorkWithFileExeption(String message) {
        super(message);
    }
}
