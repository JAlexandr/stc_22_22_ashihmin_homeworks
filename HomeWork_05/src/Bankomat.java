public class Bankomat {
    //поля
    private int moneyIn;           //денег есть
    final private int maxOut;      //макc. к выдачи
    final private int maxBalance;  // макс. кол-во денег
    private int operationCount;    //счетчик операций

    // конструктор
    public Bankomat(int moneyIn, int maxBalance, int maxOut, int operationCount) {
        this.moneyIn = moneyIn;
        this.maxOut = maxOut;
        this.maxBalance = maxBalance;
        this.operationCount = operationCount;
    }

    //Выдать деньги
    public int out(int money) {
        this.operationCount++;
        if (money > moneyIn || money > maxOut) { // не более,чем разрешено и есть в банкомате
            return 0;
        }
        moneyIn -= money;
        return money;
    }

    //Положить деньги
    public int in(int money) {
        this.operationCount++;
        if (money + moneyIn > maxBalance) { //если превышен объем
            int oldBalance = moneyIn;
            moneyIn = maxBalance;
            return money + oldBalance - moneyIn; //возвращаем сумму, которая не была положена на счет
        }
        moneyIn -= money;
        return 0;
    }
}


