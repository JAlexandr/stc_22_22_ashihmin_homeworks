public class ArraysTasksResolver {

    static void resolveTask(int[] array, ArrayTask task, int from, int to) {

        for (int j : array) {
            System.out.print(j + " ");
        }

        System.out.println("From: " + from + " to: " + to + " ");

        System.out.println("Result: " + task.resolve(array, from, to));
    }
}
