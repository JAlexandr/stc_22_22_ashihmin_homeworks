public class Main {

    public static void main(String[] args) {

        ArrayTask arrayTasks = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++)
                sum += array[i];
            return sum;

        };

        ArrayTask arrayTasksSum = (array, from, to) -> {
            int max = array[from];
            for (int i = from; i < to; i++) {
                if (array[i] > max)
                    max = array[i];
            }
            System.out.println("Max number in array: " + max);

            int sum = 0;
            if (max > 7) {
                while (max / 10 > 0) {
                    sum += max % 10;
                    max /= 10;
                }
                sum += max;
            }

            return sum;

        };

        int[] array = {12, 62, 4, 2, 100, 40, 56};
        ArraysTasksResolver.resolveTask(array, arrayTasks, 1, 3);
        ArraysTasksResolver.resolveTask(array, arrayTasksSum, 1, 3);
    }
}
