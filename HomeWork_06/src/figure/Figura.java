package figure;

public abstract class Figura { //абстрактный класс с 2мя точками
    protected int x = 0;
    protected int y = 0;

    public void move(int toX, int toY) {
        this.x = toX;
        this.y = toY;
    }

    abstract public double areaFigure();

    abstract public double perimetrFigure();
}
