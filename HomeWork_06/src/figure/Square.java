package figure;

public class Square extends Figura { //квадрат

    protected int sideSquare;

    public Square() {
    }

    @Override
    public double areaFigure() { //площадь
        return sideSquare * sideSquare;
    }

    @Override
    public double perimetrFigure() { //периметр
        return 4 * sideSquare;
    }
}
