package figure;
// элипс наследуется от круга!
public class Ellipse extends Circle {

        int maxRadius;

    public Ellipse(int radius, int maxRadius) {
        super(radius);
        this.maxRadius = maxRadius; //макс радиус
    }
    @Override
    public double areaFigure() {
        return Math.PI * radius * maxRadius;
    }
    @Override
    public double perimetrFigure() {
        return 4 * (Math.PI * radius * maxRadius + (maxRadius - radius)) / (radius + maxRadius);
    }
}
