package figure;

public class Circle extends Figura { //круг

    int radius;

    public Circle(int radius) { //конструктор
        this.radius = radius;
    }

    
    @Override
    public double areaFigure() {
        return Math.PI * radius * radius;
    }

    @Override
    public double perimetrFigure() {
        return 2 * Math.PI * radius;
    }
}

