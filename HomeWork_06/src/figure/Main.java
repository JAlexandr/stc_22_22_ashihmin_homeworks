package figure;

public class Main {
    public static void main(String[] args) {
        Square square = new Square();
        Circle circle = new Circle(2);
        Rectangle rectangle = new Rectangle();
        Ellipse ellipse = new Ellipse(5,10);
        Figura[] figures = new Figura[]{square, circle, rectangle, ellipse};
    }
}
