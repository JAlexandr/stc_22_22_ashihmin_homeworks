import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter To:"); //получили начало "от"
        int to = scanner.nextInt();

        System.out.println("Enter From:"); //получили конец "до"
        int from = scanner.nextInt();

        System.out.println("Enter Array size:"); // вводим размер массива
        int size = scanner.nextInt();
        int[] ar = new int[size];

        System.out.println("Enter number in array:"); //заполняем массив
        for (int i = 0; i < size; i++) {
            ar[i] = scanner.nextInt();
        }
        System.out.println("sum: " + sum(to, from, ar)); //выводим сумму цифр
        evenElements(ar);
    }

    public static int sum(int from, int to, int[] array) { //подаем на вход два числа (фром и ту) и массив чисел
        int sum = 0;
        if (from > to || from < 0 || to >= array.length) { // ...если from больше to   или(||) from меньше 0    или(||) from больше или равно длинне массива
            return -1;    // возвращаем -1
        }
        for (int j = from; j <= to; j++) {
            sum += array[j];
        }
        return sum; // возвращаем сумму
    }

    // Написать процедуру, которая для заданного массива выводит все его четныеэлементы
    public static void evenElements(int[] array) {
        for (int j : array) {
            if (array[j] % 2 == 0) {
                System.out.print("sum: " + array[j]);
            }
        }
    }
}