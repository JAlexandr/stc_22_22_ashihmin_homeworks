import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Enter array size:"); //размер массива

        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();

        System.out.println("Enter numbers "); // заполняем массив

        int[] array = new int[length];
        for (int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt(); //заполнили массив

        int lokMin = 0;

        for (int j = 0; j < array.length; j++) { //пробегаем по массиву
            if (j == 0) {
                if (array[j] < array[j + 1]) lokMin++;

            } else if (j == array.length - 1) {
                if (array[j] < array[j - 1]) lokMin++;

            } else if (array[j] < array[j - 1] && array[j] < array[j + 1]) lokMin++;
        }

        System.out.println("Amount lokMin: " + lokMin); //выводим
    }
}

