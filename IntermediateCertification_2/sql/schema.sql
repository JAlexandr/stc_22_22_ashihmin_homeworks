--удалить таблицу
drop table if exists the_trip;
drop table if exists cars;
drop table if exists account;

--создать
create table account(
    id_driver          bigserial primary key,
    first_name         char(20),
    last_name          char(20),
    age                integer not null,
    phone_number       char(11) unique ,
    vy                 bool,
    driving_categories char(20),
    experience         char(2)
);

alter table account
    add column rating integer check ( rating >= 0 and rating <= 5 ) default 0;

create table cars(
    id_car   bigint unique not null,
    model    char(20),
    number   char(20),
    id_owner bigint,
    foreign key (id_car) references account (id_driver)
);


create table the_trip(
    id_driver     bigint not null,
    id_car        bigint,
    travel_data   date   not null,
    trip_duration time   not null,
    foreign key (id_driver) references account (id_driver),
    foreign key (id_car) references cars (id_car)
);