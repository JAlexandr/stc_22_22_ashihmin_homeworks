--добавить
insert into account (first_name, last_name, age, phone_number, vy, driving_categories, experience)
values ('Aleksey', 'Alekseev', 18, 9001234567, true, 'B, C', 1);
insert into account (first_name, last_name, age, phone_number, vy, driving_categories, experience)
values ('Ivan', 'Ivanov', 20, 9002345678, true, 'B', 2);
insert into account (first_name, last_name, age, phone_number, vy, driving_categories, experience)
values ('Alexandr', 'Ashihmin', 34, 9123456789, true, 'B, C, D, E', 5);
insert into account (first_name, last_name, age, phone_number, vy, driving_categories, experience)
values ('Petr', 'Petrov', 22, 9003456789, true, 'C', 3);
insert into account (first_name, last_name, age, phone_number, vy, driving_categories, experience)
values ('Igor', 'Igorevich', 25, 9004567890, false, '-', 0);


insert into cars (id_car, model, number, id_owner)
values (1, 'kalina', 'х456чб174', 1);
insert into cars (id_car, model, number, id_owner)
values (2, 'Priora', 'х123чб50', 2);
insert into cars (id_car, model, number, id_owner)
values (3, 'Granta', 'х111xx186', 3);
insert into cars (id_car, model, number, id_owner)
values (4, 'Vesta', 'м852ое102', 4);
insert into cars (id_car, model, number, id_owner)
values (5, 'X5', 'a777aa777', 5);


insert into the_trip (id_driver, id_car, travel_data, trip_duration)
values (1, 5, '2022-09-01', '1:30');
insert into the_trip (id_driver, id_car, travel_data, trip_duration)
values (2, 4, '2022-10-01', '1:40');
insert into the_trip (id_driver, id_car, travel_data, trip_duration)
values (3, 3, '2022-10-15', '1:50');
insert into the_trip (id_driver, id_car, travel_data, trip_duration)
values (4, 2, '2122-11-29', '0:30');
insert into the_trip (id_driver, id_car, travel_data, trip_duration)
values (5, 1, '2022-12-05', '1:00');

--обновить
update account
set vy = false
where id_driver = 1;

--добавить колонку
alter table account
    add column rating integer check ( rating >= 0 and rating <= 5 ) default 0;

--удалить колонку
alter table account drop column rating;