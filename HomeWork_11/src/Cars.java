public class Cars {
    private String GosNumber;
    private String Model;
    private String Color;
    private Integer Mileage;
    private Integer Price;

    public Cars(String gosNumber, String model, String color, Integer mileage, Integer price) {
        this.GosNumber = gosNumber;
        this.Model = model;
        this.Color = color;
        this.Mileage = mileage;
        this.Price = price;


    }

    public String getGosNumber() {
        return GosNumber;
    }

    public String getModel() {
        return Model;
    }

    public String getColor() {
        return Color;
    }

    public Integer getMileage() {
        return Mileage;
    }

    public Integer getPrice() {
        return Price;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "GosNumber='" + GosNumber + '\'' +
                ", Model='" + Model + '\'' +
                ", Color='" + Color + '\'' +
                ", Mileage=" + Mileage +
                ", Price=" + Price +
                '}';
    }
}
