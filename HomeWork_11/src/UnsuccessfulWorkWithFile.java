import java.io.IOException;

public class UnsuccessfulWorkWithFile extends RuntimeException {
    public UnsuccessfulWorkWithFile(IOException e) {
        super(e);
    }
}
