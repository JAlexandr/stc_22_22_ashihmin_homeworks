import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;
    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }
    private static final Function<String, Cars> stringToCarsMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String gosNumber = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Cars(gosNumber, model, color, mileage, price);
    };

    @Override
    public double CamryMiddlePrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .filter(cars -> cars.getModel().equals("Camry"))
                    .mapToInt(Cars::getPrice).average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFile(e);
        }
    }

    @Override
    public Cars ColorMinPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .min(Comparator.comparingInt(Cars::getPrice))
                    .get() ;
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFile(e);
        }
    }

    @Override
    public long AmountUniqCars() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .filter(cars -> cars.getPrice() >= 700000)
                    .filter(cars -> cars.getPrice() <= 800000)
                    .map(Cars::getModel)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFile(e);
        }
    }
}