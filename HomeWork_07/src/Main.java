public class Main {

    static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) task.complete();
    }

    public static void main(String[] args) {
        Task tasks = new EvenNumbersPrintTask(1, 3);
        Task tasks1 = new OddNumbersPrintTask(3, 5);
        completeAllTasks(new Task[] {tasks,tasks1});
    }
}
