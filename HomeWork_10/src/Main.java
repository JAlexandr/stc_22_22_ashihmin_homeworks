import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String strings = "Hello Hello bye Hello bye Inno";
        String[] words = strings.split(" ");
        Map<String, Integer> map = new HashMap<>();

        for (String str : words) {
            if (map.containsKey(str)) {
                map.put(str, map.get(str) + 1);
            } else {
                map.put(str, 1);
            }
        }
        boolean current = false;
        Map.Entry<String, Integer> last = null;
        Comparator<Map.Entry<String, Integer>> comparator = Map.Entry.comparingByValue();
        for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
            if (!current || comparator.compare(stringIntegerEntry, last) > 0) {
                current = true;
                last = stringIntegerEntry;

            }
        }
        Map.Entry<String, Integer> maxValue = current ? last : null;

        System.out.println(maxValue);
    }
}

